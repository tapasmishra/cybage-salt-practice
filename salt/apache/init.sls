httpd:
  pkg:
    - latest
  service.running:
    - watch:
      - file: apache-vhost-test


apache-vhost-test:
  file.managed:
    - name: /etc/httpd/conf.d/test.conf
    - source: salt://apache/vhost_conf/test.vhost
    - require:
      - pkg: httpd

/var/www/html/index.html:
  file.managed:
    - source: salt://apache/sample_app/index.html

/var/www/html/404.html:
  file.managed:
    - source: salt://apache/sample_app/404.html